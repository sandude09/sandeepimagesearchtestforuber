package com.demo.sandeep.viewmodel;

import android.arch.lifecycle.LiveData;

import com.demo.sandeep.di.rx.SchedulersRule;
import com.demo.sandeep.di.rx.TestSchedulerProvider;
import com.demo.sandeep.model.Item;
import com.demo.sandeep.model.data.DataManager;
import com.demo.sandeep.model.pojos.FlickrImagesResponse;

import org.junit.Assume;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import io.reactivex.Observable;
import io.reactivex.schedulers.TestScheduler;

import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;


public class MainActivityVMTest {


    @Rule
    public SchedulersRule schedulersRule = new SchedulersRule();

    @Mock
    DataManager mockDataManager;

    private MainActivityVM mMainActivityVM;

    private TestScheduler mTestScheduler;

    private LiveData<List<Item>> liveData;

    @BeforeClass
    public static void onlyOnce() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mTestScheduler = new TestScheduler();
        TestSchedulerProvider testSchedulerProvider = new TestSchedulerProvider(mTestScheduler);
        mMainActivityVM = new MainActivityVM(mockDataManager, testSchedulerProvider);
        liveData = mMainActivityVM.getLiveData();
    }

    @Test
    public void getImagesTest_WhenText_ReturnImageList()
    {

        FlickrImagesResponse mFlickrImagesResponse = new FlickrImagesResponse();
        ArrayList<Item> dummyList = new ArrayList<>();
        for(int i=0;i<5;i++) {
            Item item = new Item();
            dummyList.add(item);
        }

        mFlickrImagesResponse.setDummyPhotoList(dummyList);

        Mockito.when(mockDataManager.fetchImageList("Kitten",1)).thenReturn( Observable.just(mFlickrImagesResponse));

        mMainActivityVM.getImages( "Kitten");
        mTestScheduler.triggerActions();

       /* imageListSubscriber.assertNoErrors();
        imageListSubscriber.assertCompleted();*/
        assertNotNull(liveData);
        assertNotNull(liveData.getValue());
        assert(liveData.getValue().size() ==5);
    }

}
