package com.demo.sandeep;

import android.app.Activity;
import android.app.Application;

import com.demo.sandeep.di.DaggerMyAppComponent;
import com.demo.sandeep.di.MyAppComponent;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/*
 * Created by sandeepgupta on 15/07/18.
 */

public class MyApplication extends Application implements HasActivityInjector {

    MyAppComponent myAppComponent;
    @Inject
    protected DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return activityDispatchingAndroidInjector;
    }
    @Override
    public void onCreate()
    {
        super.onCreate();


        myAppComponent = initializeAppComponent();
        myAppComponent.inject(this);
}

    protected MyAppComponent initializeAppComponent() {
        return DaggerMyAppComponent.builder()
                .application(this)
                .build();
    }

    protected void setComponent(MyAppComponent app) {
        this.myAppComponent =app;
    }
}

