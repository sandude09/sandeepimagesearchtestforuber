package com.demo.sandeep.model.data;

import com.demo.sandeep.model.pojos.BaseResponse;
import com.demo.sandeep.model.pojos.FlickrImagesResponse;

import io.reactivex.Observable;
import retrofit2.Response;

public interface DataManager {

    Observable<FlickrImagesResponse> fetchImageList(String search_text, int page);
}
