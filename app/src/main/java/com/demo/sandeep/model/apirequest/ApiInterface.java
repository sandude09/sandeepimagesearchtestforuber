package com.demo.sandeep.model.apirequest;

import com.demo.sandeep.model.pojos.FlickrImagesResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/*
 * Created by sandeepgupta on 15/07/18.
 */

public interface ApiInterface
{
    @GET("services/rest/")
    Observable<FlickrImagesResponse> getFlickrImageList(@Query("method") String methodname , @Query("api_key") String apikey,@Query("format") String format , @Query("nojsoncallback") int nojsoncallback,@Query("safe_search") int safe_search , @Query("text") String text,@Query("page") int page);//@Url We will pass the Url using @Url tag as it will ignore the base url

}
