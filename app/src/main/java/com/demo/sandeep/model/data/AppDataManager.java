package com.demo.sandeep.model.data;

import com.demo.sandeep.extras.ConstantData;
import com.demo.sandeep.model.apirequest.ApiInterface;
import com.demo.sandeep.model.pojos.BaseResponse;
import com.demo.sandeep.model.pojos.FlickrImagesResponse;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;

public class AppDataManager implements  DataManager{

    private ApiInterface apiService;

    public AppDataManager(ApiInterface apiInterface){
        this.apiService =apiInterface;
    }

    @Override
    public Observable<FlickrImagesResponse> fetchImageList(String search_text, int page)
    {
        return apiService.getFlickrImageList("flickr.photos.getRecent",  ConstantData.api_key, "json" ,1,1, search_text, page );

    }
}
