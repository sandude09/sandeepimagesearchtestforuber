package com.demo.sandeep.model


/*
 * Created by sandeepgupta on 15/07/18.
 */

/*
to-do : Migrate all files to java if sufficient time not left to convert all files into Kotlin
 */
class Item
{
    /*id: "42936301841",
    owner: "13081496@N02",
    secret: "eda0e4e39d",
    server: "1766",
    farm: 2,
    title: "20180621-IMG_8181",
    ispublic: 1,
    isfriend: 0,
    isfamily: 0*/

    var id : String? = null
    var owner  : String? = null
    var secret  : String? = null
    var server  : String? = null
    var farm  : Int? = null
    var title : String? = null
    var ispublic :  Int?= null
    var isfriend :  Int?= null
    var isfamily :  Int?= null


}