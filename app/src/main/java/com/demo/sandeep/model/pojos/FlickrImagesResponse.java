package com.demo.sandeep.model.pojos;

import com.demo.sandeep.model.Item;

import java.util.List;

/**
 * Created by sandeepgupta on 15/07/18.
 */

public class FlickrImagesResponse  extends  BaseResponse{


    private Photos photos;
    public FlickrImagesResponse() {
        photos = new Photos();
    }
    public void setDummyPhotoList(List<Item> photos) {
        this.photos.setPhoto(photos);
    }
    public Photos getPhotos( ) {
        return photos;
    }
    

    public static class Photos
    {
        private int page;
        private int pages;
        private String total;
        private int perpage;
        private List<Item> photo;

        public List<Item> getPhoto() {
            return photo;
        }

        public void setPhoto(List<Item> photo) {
            photo = photo;
        }

        public Photos getPhotos() {
            return this;
        }

       

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            page = page;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            pages = pages;
        }

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            total = total;
        }

        public int getPerpage() {
            return perpage;
        }

        public void setPerpage(int perpage) {
            perpage = perpage;
        }
    }
}
