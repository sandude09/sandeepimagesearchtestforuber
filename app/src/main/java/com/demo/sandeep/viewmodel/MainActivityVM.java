package com.demo.sandeep.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.demo.sandeep.model.Item;
import com.demo.sandeep.di.rx.SchedulerProvider;
import com.demo.sandeep.extras.ConstantData;
import com.demo.sandeep.model.apirequest.ApiInterface;
import com.demo.sandeep.model.data.AppDataManager;
import com.demo.sandeep.model.data.DataManager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/*
 * Created by sandeepgupta on 15/07/18.
 */

public class MainActivityVM extends ViewModel
{

    private MutableLiveData<List<Item>> flickrImageList;
    private Disposable disposable;
    private int currentPage = -1;
    private String totalCount = "0";
    private int perPageCount = 0;


    private DataManager appDataManager;
    private SchedulerProvider schedulers;

    public MainActivityVM(DataManager dataManager, SchedulerProvider schedulers){
        this.appDataManager = dataManager;
        this.schedulers =schedulers;
        flickrImageList = new MutableLiveData<>();

    }

    public void cleanup() {
        totalCount = "0";
        currentPage = -1;

    }

    public LiveData<List<Item>> getLiveData()
    {
        return flickrImageList;
    }

    public void getImages(String search_text)
    {
        appDataManager.fetchImageList(search_text, (currentPage+1) )
     .subscribeOn(schedulers.io())
                .observeOn(schedulers.ui()).map(flickrImagesResponseResponse -> {
                    currentPage = flickrImagesResponseResponse.getPhotos().getPage();
                    totalCount = flickrImagesResponseResponse.getPhotos().getTotal();
                    perPageCount = flickrImagesResponseResponse.getPhotos().getPerpage();

                    return flickrImagesResponseResponse.getPhotos().getPhoto();
                })
                .subscribeWith(new Observer<List<Item>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable= d;
                           // Log.d("TAG" , "onSubscribe: " );
                        }

                        @Override
                        public void onNext(List<Item> value) {
                            //Log.d("TAG" , "onNext - list IternaryModel size: " + value.size());
                            flickrImageList.setValue(value);
                        }

                        @Override
                        public void onError(Throwable e) {
                           // Log.d("TAG" , "onError: "  + e.getMessage());
                        }

                        @Override
                        public void onComplete() {
                            Log.d("TAG" , "onComplete: " );
                        }
                    });
    }


    @Override
    protected void onCleared()
    {
        // Note: We do not need to call super.onCleared() because the base implementation is empty.
        if(disposable != null && !disposable.isDisposed())
        {
            disposable.dispose();// To dispose or unSuscribe the RxJava Observer
        }
    }

    public int getTotalCount()
    {
        return Integer.parseInt(totalCount);
    }

    public int getPerPageCount()
    {
        return perPageCount;
    }
}
