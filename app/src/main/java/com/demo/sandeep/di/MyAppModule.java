package com.demo.sandeep.di;

import android.app.Application;
import android.content.Context;

import com.demo.sandeep.di.rx.AppSchedulerProvider;
import com.demo.sandeep.di.rx.SchedulerProvider;
import com.demo.sandeep.extras.ConstantData;
import com.demo.sandeep.model.apirequest.ApiInterface;
import com.demo.sandeep.model.data.AppDataManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by sandeepgupta on 15/07/18.
 */
@Module
public class MyAppModule {

    @Provides
    @Singleton
    @AppContext
    Context provideContext(Application application) {
        return application;
    }


    @Provides
    @Singleton
    ApiInterface provideInterface(Retrofit client) {
        return client.create(ApiInterface.class);
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();


        return new Retrofit.Builder()
                .baseUrl(ConstantData.domain)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build();
    }

    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new AppSchedulerProvider();
    }

    @Provides
    AppDataManager provideAppDataMager(ApiInterface apiInterface)
    {
        return new AppDataManager(apiInterface);
    }
}