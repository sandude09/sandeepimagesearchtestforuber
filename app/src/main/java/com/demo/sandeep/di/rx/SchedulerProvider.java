package com.demo.sandeep.di.rx;

import io.reactivex.Scheduler;

/**
 * Created by sandeepgupta on 15/07/18.
 */

public interface SchedulerProvider {

    Scheduler computation();

    Scheduler io();

    Scheduler ui();
}
