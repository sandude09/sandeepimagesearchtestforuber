package com.demo.sandeep.di;

import android.app.Application;

import com.demo.sandeep.MyApplication;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;


/**
 * Created by sandeepgupta on 15/07/18.
 */



@Singleton
@Component(modules = {AndroidSupportInjectionModule.class,MyAppModule.class,
        ActivityBuilder.class
})
public interface MyAppComponent  {

    void inject(MyApplication application);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        MyAppComponent build();
    }

}