package com.demo.sandeep.di;

import com.demo.sandeep.di.rx.SchedulerProvider;
import com.demo.sandeep.model.apirequest.ApiInterface;
import com.demo.sandeep.model.data.AppDataManager;
import com.demo.sandeep.model.data.DataManager;
import com.demo.sandeep.viewmodel.MainActivityVM;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sandeepgupta on 15/07/18.
 */
@Module
public class MainActivityModule {

    @Provides
    MainActivityVM provideMainActivityVM(AppDataManager appDataManger, SchedulerProvider schedulerProvider) {
        return new MainActivityVM(appDataManger, schedulerProvider);
    }
}
