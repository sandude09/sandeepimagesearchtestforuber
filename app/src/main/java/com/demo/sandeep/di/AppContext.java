package com.demo.sandeep.di;

import java.lang.annotation.Retention;

import javax.inject.Qualifier;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by sandeepgupta on 15/07/18.
 */

@Qualifier
@Retention(RUNTIME)
public @interface AppContext {
}
