package com.demo.sandeep.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.demo.sandeep.view.MainActivity;
import com.demo.sandeep.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {
    private MemoryCache memoryCache;
    private FileCache fileCache;
    private Map imageViews = Collections.synchronizedMap(new WeakHashMap());
    private Drawable mStubDrawable;
    private ExecutorService executorService;

    public ImageLoader(Context context) {
        fileCache = new FileCache(context);
        executorService=Executors.newFixedThreadPool(4);
        init(context);
    }

    private void init(Context context) {
        memoryCache=new MemoryCache();
        mStubDrawable = context.getResources().getDrawable(R.drawable.uber_placeholder);
    }

    public void displayImage(String url, ImageView imageView)
    {
        Bitmap bitmap= null;
        imageViews.put(imageView, url);
        if (url != null && url.length() > 0)
         bitmap=memoryCache.get(url);
        if(bitmap!=null) {
            imageView.setImageBitmap(bitmap);
        }
        else
        {
            if (url != null && url.length() > 0) {
                queuePhoto(url, imageView);

            }
            imageView.setImageDrawable(mStubDrawable);
        }
    }

    private void queuePhoto(String url, ImageView imageView) {
       // new LoadBitmapTask().execute(url, imageView);

        executorService.execute(new PhotosLoader(new PhotoToLoad(url, imageView)));
    }

    /**
     * Search for the image in the device, then in the web
     * @param url
     * @return
     */

    private Bitmap getBitmap(String url)
    {
        File f=fileCache.getFile(url);

        //from SD cache
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;
        URL myfileurl =null;
        try {
            myfileurl= new URL(url);

        }
        catch (MalformedURLException e)
        {
            e.printStackTrace();
            return null;
        }

        try
        {
            HttpURLConnection conn= (HttpURLConnection)myfileurl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            BitmapFactory.Options options = new BitmapFactory.Options();


            return BitmapFactory.decodeStream(is,null,options);

        } catch (Throwable ex){
            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }

    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }


    private class PhotoToLoad {
        public String url;
        public ImageView imageView;

        public PhotoToLoad(String u, ImageView i) {
            url = u;
            imageView = i;
        }
    }

    private boolean imageViewReused(PhotoToLoad photoToLoad) {
        //tag used here
        String tag = (String)imageViews.get(photoToLoad.imageView);
        if (tag == null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }

        @Override
        public void run() {
            if(imageViewReused(photoToLoad)) {
                return;
            }
            Bitmap bmp=getBitmap(photoToLoad.url);
            if (bmp == null) {
                return;
            }
            memoryCache.put(photoToLoad.url, bmp);
            if(imageViewReused(photoToLoad)) {
                return;
            }


            BitmapDisplayer bd =new BitmapDisplayer(bmp,photoToLoad);
            MainActivity a=(MainActivity)photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
        public void run()
        {
            if(imageViewReused(photoToLoad)) {
                return;
            }
            if(bitmap!=null) {
                photoToLoad.imageView.setImageBitmap(bitmap);

            }else {
                photoToLoad.imageView.setImageDrawable(mStubDrawable);
            }
        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }


}
