package com.demo.sandeep.view;


import android.arch.lifecycle.ViewModelProvider;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.sandeep.BuildConfig;
import com.demo.sandeep.R;
import com.demo.sandeep.extras.ConnectivityReceiver;
import com.demo.sandeep.imageloader.ImageLoader;
import com.demo.sandeep.model.Item;
import com.demo.sandeep.view.adapter.ItemArrayAdapter;
import com.demo.sandeep.viewmodel.MainActivityVM;
import com.demo.sandeep.databinding.ActivityMainListBinding;


import java.util.ArrayList;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/*
 * Created by sandeepgupta on 15/07/18.
 */


public class MainActivity extends AppCompatActivity
{
    static
    {   // To provide support of vector icon below sdk version 21.
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    ViewModelProvider.Factory viewModelFactory;

    private ActivityMainListBinding mViewDataBinding;

    // Labels
    private ItemArrayAdapter itemListAdapter;
    private ArrayList<Item> itemArrayList;
    private GridLayoutManager gridLayoutManager;
    private String queryString = null;

    @Inject
    MainActivityVM mainActivityVM;
    private boolean mLoadingItems = true;



    private static boolean isIdle = true;
    private String loadingTag = "MainActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AndroidInjection.inject(this);
        mViewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_list);
        mViewDataBinding.executePendingBindings();
        mViewDataBinding.sortBarLayout.setVisibility(View.GONE);
        initRecyclerView();
        mViewDataBinding.imagesRcylv.addOnScrollListener(scrollListener);
        setIdlingResource(true,MainActivity.class.getSimpleName());
    }

    private void initRecyclerView()
    {
        /* Intialization of ArrayList */
        itemArrayList = new ArrayList<Item>();
        itemListAdapter = new ItemArrayAdapter(itemArrayList, new ImageLoader(this));
        gridLayoutManager = new GridLayoutManager(this, 3);
        gridLayoutManager.setOrientation(gridLayoutManager.VERTICAL);
        mViewDataBinding.imagesRcylv.setLayoutManager(gridLayoutManager);
        mViewDataBinding.imagesRcylv.setAdapter(itemListAdapter);
        mainActivityVM.getLiveData().observe(this, imageList -> {
            dimissProgress();
            mLoadingItems = false;
            int sizeBefore = itemArrayList.size();
            itemArrayList.addAll(imageList);
            itemListAdapter.notifyItemRangeChanged(sizeBefore, itemArrayList.size() - 1);
            TextView count = mViewDataBinding.sortBarLayout.findViewById(R.id.count_txt);
            if (mainActivityVM.getTotalCount() > 0) {
                count.setVisibility(View.VISIBLE);
                count.setText("" + itemArrayList.size() + " of " + mainActivityVM.getTotalCount() + " results");
            } else {
                count.setVisibility(View.GONE);
            }
        });

        Toast.makeText(this.getApplication(), R.string.startSearching, Toast.LENGTH_SHORT).show();
    }


    private void dimissProgress()
    {
        if(mViewDataBinding.progressBar != null) {
            mViewDataBinding.progressBar.setVisibility(View.GONE);
            mViewDataBinding.sortBarLayout.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        // to avoid any memory leaks as context has been passed
        mViewDataBinding.imagesRcylv.setAdapter(null);
    }

    // To-be used in espresso tests.
    public boolean isIdle() {
        return isIdle;
    }
    // To-be used in espresso tests.
    public String getLoadingTag(){
        return loadingTag;
    }

    protected void setIdlingResource(boolean idle, String tag) {

        if (BuildConfig.DEBUG){
            isIdle = idle;
            loadingTag = tag;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);

        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void startSearch()
    {
        if(!TextUtils.isEmpty(queryString)) {
            if (ConnectivityReceiver.isNetworkAvailable(this.getApplication())) {
                mViewDataBinding.progressBar.setVisibility(View.VISIBLE);
                mainActivityVM.getImages(queryString);
            } else {
                Toast.makeText(this.getApplication(), R.string.connectWifiDataConn, Toast.LENGTH_SHORT).show();
            }
        } else {

            Toast.makeText(this.getApplication(), R.string.startSearching, Toast.LENGTH_SHORT).show();
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!TextUtils.isEmpty(query)) {

                    if(TextUtils.isEmpty(queryString))
                    {
                        queryString = query;
                        startSearch();
                    }
                    else {
                        if (!TextUtils.isEmpty(queryString) && !queryString.equalsIgnoreCase(query)) {
                            queryString = query;
                            itemArrayList.clear();
                            itemListAdapter.notifyDataSetChanged();
                            mainActivityVM.cleanup();
                            mainActivityVM.getImages(queryString);
                        }

                    }
                }

                    return true;

            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });
    }

    RecyclerView.OnScrollListener scrollListener  = new RecyclerView.OnScrollListener() {
        int mPreviousTotal=0, mOnScreenItems=0, mTotalItemsInList=0,mVisibleThreshold=10,mFirstVisibleItem=0 ;
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            mOnScreenItems = mViewDataBinding.imagesRcylv.getChildCount();
            mTotalItemsInList = gridLayoutManager.getItemCount();
            mFirstVisibleItem = gridLayoutManager.findFirstVisibleItemPosition();

            if(mTotalItemsInList<=mainActivityVM.getTotalCount()-mainActivityVM.getPerPageCount()) {

                if (mLoadingItems) {
                    if (mTotalItemsInList > mPreviousTotal) {
                        mLoadingItems = false;
                        mPreviousTotal = mTotalItemsInList;
                    }
                }

                if (!mLoadingItems && (mTotalItemsInList - mOnScreenItems) <= (mFirstVisibleItem + mVisibleThreshold)) {
                    if (mTotalItemsInList < mainActivityVM.getTotalCount()) {
                        mainActivityVM.getImages(queryString);
                        mLoadingItems = true;
                    }

                }
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

    };
}
