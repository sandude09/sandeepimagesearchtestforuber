package com.demo.sandeep.view.adapter

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.demo.sandeep.model.Item
import com.demo.sandeep.R
import com.demo.sandeep.imageloader.ImageLoader
import kotlinx.android.synthetic.main.list_item.view.*
import kotlinx.android.synthetic.main.list_item_progressbar.view.*
import java.util.ArrayList

/*
 * Created by sandeepgupta on 15/07/18.
 */

/*
to-do : Migrate all files to java if sufficient time not left to convert all files into Kotlin
 */
class ItemArrayAdapter(private var itemList: ArrayList<Item> = ArrayList(), private var  imageLoader : ImageLoader) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {



    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

            val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
            return RegularViewHolder(view)

    }

    // load data in each row element
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        holder as RegularViewHolder
        imageLoader.displayImage(getImagePath(position) , holder.item.imageview)


    }

    fun  getImagePath(pos : Int) : String {

        var path = "http://farm{farm}.static.flickr.com/{server}/{id}_{secret}.jpg"
       path = path.replace("{farm}" , itemList.get(pos).farm.toString(),false)
        .replace("{server}" , itemList.get(pos)!!.server!!,false)
        .replace("{id}" , itemList.get(pos).id!!,false)
        .replace("{secret}" , itemList.get(pos).secret!!,false)

        return path;
    }


  /*  // this is required to be called right before loading more items
    fun addFooter() {
        Log.d("endlessscroll", "addFooter")
        if (!isLoading()) {
            itemList.add(Item("Footer", 1))
            notifyItemInserted(itemList.size - 1)
        }
    }*/

    // this is required to be called right after finish loading the items
        /*fun removeFooter() {
            Log.d("endlessscroll", "removeFooter")
            if (isLoading()) {
                itemList.removeAt(itemList.size - 1)
                notifyItemRemoved(itemList.size - 1)
            }
        }
    */


    fun addItems(items : ArrayList<Item>) {
        val lastPos = itemList.size - 1
        itemList.addAll(items)
        notifyItemRangeInserted(lastPos+1, items.size)
    }

    inner class RegularViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: ImageView

        init {
            item = itemView.findViewById<View>(R.id.imageview) as ImageView
        }

    }
/*
    inner class FooterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var progressBar = itemView.progressbar
    }*/
}
