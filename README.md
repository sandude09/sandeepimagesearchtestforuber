# Completed
- Android Image Search UI with infinite scrolling
- Image Caching mechanism
- MVVM design pattern + Rx + Dagger2
- Unit test cases
- Few files into Kotlin files


# Improvement scope
- Add more junit test cases and espresso tests
- Documentation + cleanup
- Improvement in Caching
- Migration to Kotlin
